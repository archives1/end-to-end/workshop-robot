# Robot


## Requirements

ค้นหาคำว่า "ไฟไหม้กิ่งแก้ว" จะได้คำว่า "ด่วน ไฟไหม้โรงงานกิ่งแก้ว เกิดปะทุอีกรอบ"

1. เลือก browser เข้าสู่เว็บ google.com

2. ค้นหาคำว่า ไฟไหม้กิ่งแก้ว

3. กด Enter

4. ต้องการเจอ "ด่วน ไฟไหม้โรงงานกิ่งแก้ว เกิดปะทุอีกรอบ" 






#### Install Library

```bash
pip3 install robotframework-seleniumlibrary==5.1.3
```

    
### Build image docker 

```bash

docker build -t tokdev/workshop-robot:0.0.1 .

```

### Create Dcoker-compose


```bash
envsubst < docker-compose-template.yml > docker-compose.yml

```


### Check Docker-compose format


```bash
docker-compose -f docker-compose.yml config

```


```bash
docker run --rm \
           -e USERNAME="Ipatios Asmanidis" \
           --net=host \
           -v "$PWD/output":/output \
           -v "$PWD/google":/suites \
           -v "$PWD/scripts":/scripts \
           -v "$PWD/reports":/reports \
           --security-opt seccomp:unconfined \
           --shm-size "256M" \
           tokdev/robot-framework:0.0.1-ubuntu18.04
```



