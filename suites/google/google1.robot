*** Settings ***
Library    SeleniumLibrary
Library     XvfbRobot

Suite Setup    Start Virtual Display    1920    1080

*** Variables ***

*** Test Cases ***
ค้นหาคำว่า "ไฟไหม้กิ่งแก้ว" จะได้คำว่า "โรงงานกิ่งแก้วไฟไหม้"
    เลือก browser เข้าสู่เว็บ google.com
    ค้นหาคำว่า ไฟไหม้กิ่งแก้ว
    กด Enter
    ต้องการเจอ "โรงงานกิ่งแก้วไฟไหม้"

*** Keywords ***

Open Chrome Browser
    [Arguments]    ${URL}    ${TMP_PATH}=/tmp
    ${options}    Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    Call Method    ${options}    add_argument    --headless
    # Call Method    ${options}    add_argument    --disable-gpu
    Call Method    ${options}    add_argument    --no-sandbox
    # Call Method    ${options}    add_argument    --disable-dev-shm-usage
    # Call Method    ${options}    add_argument    --start-maximized
    ${prefs}    Create Dictionary    download.default_directory=${TMP_PATH}
    Call Method    ${options}    add_experimental_option    prefs    ${prefs}
    Create Webdriver    Chrome    chrome_options=${options}
    GoTo    ${URL}

เลือก browser เข้าสู่เว็บ google.com
    # Open Chrome Browser     http://www.google.com
    Open Browser   url=http://www.google.com    browser=firefox

ค้นหาคำว่า ไฟไหม้กิ่งแก้ว
    Input Text    name=q    ไฟไหม้กิ่งแก้ว

กด Enter
    Press Keys    name=q    ENTER

ต้องการเจอ "โรงงานกิ่งแก้วไฟไหม้"
    Wait Until Page Contains    โรงงานกิ่งแก้วไฟไหม้

