*** Settings ***
Library    SeleniumLibrary

*** Variables ***
${URL}    http://www.google.com
${BROWSER}    chrome

*** Test Cases ***
ค้นหาคำว่า "ล็อกดาวน์กรุงเทพ" จะได้คำว่า "ยกระดับมาตรการ กทม.-ปริมณฑล รวม 6 จังหวัด เดินทางข้ามจังหวัด"
    เลือก browser เข้าสู่เว็บ google.com
    ค้นหาคำว่า    ล็อกดาวน์กรุงเทพ
    กด Enter
    ต้องการเจอ    ยกระดับมาตรการ กทม.-ปริมณฑล รวม 6 จังหวัด เดินทางข้ามจังหวัด

ค้นหาคำว่า "ไฟไหม้กิ่งแก้ว" จะได้คำว่า "ด่วน ไฟไหม้โรงงานกิ่งแก้ว เกิดปะทุอีกรอบ"
    เลือก browser เข้าสู่เว็บ google.com
    ค้นหาคำว่า    ไฟไหม้กิ่งแก้ว
    กด Enter
    ต้องการเจอ    ด่วน ไฟไหม้โรงงานกิ่งแก้ว เกิดปะทุอีกรอบ

ค้นหาคำว่า "เงินเทอร์โบ" จะได้คำว่า "https://www.turbo.co.th"
    เลือก browser เข้าสู่เว็บ google.com
    ค้นหาคำว่า    เงินเทอร์โบ
    กด Enter
    ต้องการเจอ    https://www.turbo.co.th
    

*** Keywords ***
เลือก browser เข้าสู่เว็บ google.com
    Open Browser   url=${URL}    browser=${BROWSER}

กด Enter
    Press Keys    name=q    ENTER

ค้นหาคำว่า
    [Arguments]    ${WORK_FIND}
    Input Text    name=q    ${WORK_FIND}

ต้องการเจอ
    [Arguments]    ${EXPECTED_WORD}
    Page Should Contain    ${EXPECTED_WORD}

    