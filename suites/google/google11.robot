*** Settings ***
Library    SeleniumLibrary

*** Variables ***

*** Test Cases ***
ค้นหาคำว่า "ไฟไหม้กิ่งแก้ว" จะได้คำว่า "ด่วน ไฟไหม้โรงงานกิ่งแก้ว เกิดปะทุอีกรอบ"
    เลือก browser เข้าสู่เว็บ google.com
    ค้นหาคำว่า ไฟไหม้กิ่งแก้ว
    กด Enter
    ต้องการเจอ "ด่วน ไฟไหม้โรงงานกิ่งแก้ว เกิดปะทุอีกรอบ"

*** Keywords ***
เลือก browser เข้าสู่เว็บ google.com
    Open Browser   url=http://www.google.com    browser=chrome

ค้นหาคำว่า ไฟไหม้กิ่งแก้ว
    Input Text    name=q    ไฟไหม้กิ่งแก้ว

กด Enter
    Press Keys    name=q    ENTER

ต้องการเจอ "ด่วน ไฟไหม้โรงงานกิ่งแก้ว เกิดปะทุอีกรอบ"
    Page Should Contain    ไฟไหม้โรงงานกิ่งแก้ว เช้านี้เริ่มมีควันดำทะมึนพวยพุ่งขึ้นฟ้าอีกครั้ง
