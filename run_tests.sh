#!/usr/bin/env bash

docker build -t tokdev/workshop-robot:0.0.1 .
docker run --rm \
           -e USERNAME="Ipatios Asmanidis" \
           --net=host \
           -v "$PWD/output:/output" \
           -v "$PWD/reports:/reports" \
           --security-opt seccomp:unconfined \
           --shm-size "256M" \
           tokdev/workshop-robot:0.0.1